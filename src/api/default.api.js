export async function home(req, res) {
  res.json({ message: 'CatHub REST API v1.0' });
}

export async function about(req, res) {
  res.json({ message: 'CatHub REST API v1.0 is an Open Source Project aim to teach how to build REST API with Node.js.' });
}

import debug from 'debug';

import User from '../models/user.model';
import { verify } from '../util/jwt';

const log = debug('cathub:auth-middleware');

const errorMessage = 'Invalid access token';

export function checkAuth(type = 'strong') {
  return async (req, { bad }, next) => {
    const accessToken = req.get('authorization');
    if (!accessToken) {
      if (type === 'weak') {
        next();
      } else {
        bad(errorMessage);
      }
    } else {
      try {
        const decoded = await verify(accessToken);
        delete decoded.iat;
        delete decoded.exp;
        delete decoded.iss;
        delete decoded.__v; // eslint-disable-line

        const user = await User.findById(decoded._id); // eslint-disable-line
        if (!user) bad(errorMessage);

        req.user = user;
        next();
      } catch (e) {
        if (type === 'weak') {
          next();
        } else {
          log(e);
          bad(errorMessage);
        }
      }
    }
  };
}

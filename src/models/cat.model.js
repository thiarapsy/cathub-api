import mongoose, { Schema } from 'mongoose'; // cool kids

const catSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    vote: Number,
    upvoteCount: { type: Number, default: 0 },
    downvoteCount: { type: Number, default: 0 },
    imageUrl: {
      type: String,
      required: true,
    },
  },
  { timestamps: { createdAt: 'createdAt' } },
);

export default mongoose.model('Cat', catSchema);

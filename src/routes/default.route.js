import { createRouter, HttpMethods } from 'express-router-helper';

import { about, home } from '../api/default.api';
import { uniqueUsername } from '../util/validation.rules';

export default createRouter({
  prefix: '',
  routes: [
    {
      method: HttpMethods.GET,
      path: '',
      middleware: [],
      handler: home,
    },
    {
      method: HttpMethods.GET,
      path: 'about',
      middleware: [],
      handler: about,
    },
  ],
});

import debug from 'debug';
import xray from 'x-ray';
import { filterAndInsert as add } from '../util/scrape-filter';

const x = xray();
const log = debug('cathub:catgifpage');

const url = 'https://www.catgifpage.com/';
const scope = '#container .container .post';

function scrape() {
  return new Promise((resolve, reject) => {
    x(url, scope, [
      {
        title: 'h2',
        imageUrl: 'a img@src',
      },
    ]).paginate('.pagination .next a@href')((err, cats) => {
      if (err) throw reject(err);
      resolve(cats);
    });
  });
}

export async function scrapeGifCat(req, { ok }) {
  const cats = await scrape();
  await add(cats);
  ok();
}

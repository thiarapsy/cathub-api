// Imports the Google Cloud client library
import vision from '@google-cloud/vision';

// Creates a client
const client = new vision.ImageAnnotatorClient();

// Performs label detection on the image file
export async function detectLabel(filePath) {
  return client.labelDetection(filePath);
}

// HOW TO AUTHENTICATE
// 1. Set up environment variable
//  if you're on Windows, run this on your cmd: set GOOGLE_APPLICATION_CREDENTIALS=[PATH]
//    replace [PATH] with the location of the JSON file that contains the service account key; in our case the file is named 'CatHub-8a75a5c3f0a8.json'
//    for example, if you put the file on your C: drive, the command should look like this: set GOOGLE_APPLICATION_CREDENTIALS=C:\CatHub-8a75a5c3f0a8.json
//  if you're on macOS or Linux run this on your bash: export GOOGLE_APPLICATION_CREDENTIALS="[PATH]"
//    replace [PATH] with the location of the JSON file that contains the service account key

import debug from 'debug';
import nodemailer from 'nodemailer';
import randomstring from 'randomstring';

const log = debug('cathub:mailer');

function generateRandomString() {
  return randomstring.generate({
    capitalization: 'uppercase',
    length: 6,
    charset: 'alphanumeric',
  });
}

function sendMail(email, code) {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'tedpsy94@gmail.com',
      pass: 'regularPass',
    },
  });
  const mailOptions = {
    from: '"CatHub Team" <tedpsy94@gmail.com>',
    to: email,
    subject: 'CatHub - Login Code',
    text: `Your login code is ${code}.`,
    html: `
      <p>Hi there,</p>
      <br>
      <p>Your login code is <b>${code}</b></p>
      <br>
      <p>CatHub Team</p>
    `,
  };
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (err, result) => {
      if (err) {
        log('Send Mail Failed', err);
        return reject(err);
      }
      log('Message sent: %s', result.messageId);
      log('Preview URL: %s', nodemailer.getTestMessageUrl(result));
      return resolve();
    });
  });
}

export async function sendRequestLoginCode(email) {
  const code = generateRandomString();
  await sendMail(email, code);
  return code;
}

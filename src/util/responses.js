export function responses() {
  return (req, res, next) => {
    res.ok = function ok(response) {
      if (typeof response === 'string') {
        res.json({ message: response || 'OK' });
      } else if (typeof response === 'object') {
        res.json({ message: 'OK', ...response }); // res.json(Object.assign({ message: 'OK' }, response));
      } else {
        res.json({ message: 'OK' });
      }
    };
    res.bad = function bad(response = 'Bad Request') {
      let e;
      if (typeof response === 'string') {
        e = new Error(response);
      } else if (typeof response === 'object') {
        e = new Error(response.message || 'Bad Request');
        e.response = response;
      } else {
        e = new Error('res.bad expects a string or an object');
      }
      e.status = 400;
      throw e;
    };
    next();
  };
}

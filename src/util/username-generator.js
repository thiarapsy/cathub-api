import User from '../models/user.model';

export async function generateUsername(email) {
  const username = email
    .toLowerCase()
    .slice(0, email.indexOf('@'))
    .replace('.', '');
  const emailHoster = email.slice(
    email.indexOf('@') + 1,
    email.indexOf('.com'),
  );
  const user = await User.findOne({ username });
  if (user) {
    return `${username}.${emailHoster}`;
  }
  return username;
}

// ted@ph.com ted@cathub.com
